import { ConversationsListArguments, ConversationsListResponse } from '@slack/web-api'
import {
  app,
  list
} from '../util'

// List all channels on the server
export const job = async (base?: ConversationsListArguments): Promise<NonNullable<ConversationsListResponse['channels']>> => await list(
  'channels',
  async (extra?: ConversationsListArguments) => await app.client.conversations.list({
    ...base,
    ...extra
  })
)
