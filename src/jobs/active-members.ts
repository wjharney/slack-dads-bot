import { app, ensureEnv, list } from '../util'

const activeChannel = ensureEnv('CHANNEL_FOR_ACTIVE_MEMBERS')

export const job = async (): Promise<string[]> => await list(
  'members',
  async (extra) => await app.client.conversations.members({ channel: activeChannel, ...extra })
)
