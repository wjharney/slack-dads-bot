import { ConversationsHistoryArguments, ConversationsHistoryResponse, Method } from '@slack/web-api'
import { Message } from '@slack/web-api/dist/response/ConversationsHistoryResponse'
import { Channel } from '@slack/web-api/dist/response/ConversationsListResponse'
import {
  app,
  list,
  token
} from '../util'
import { job as getChannels } from './channels'

export interface MessageWithChannel extends Message {
  channel: string
}

async function getMessages(id: string): Promise<MessageWithChannel[]> {
  const conversationsHistory: Method<Omit<ConversationsHistoryArguments, 'channel'>, ConversationsHistoryResponse> =
  async (payload) => await app.client.conversations.history({ ...payload, channel: id })

  const allMessages: Message[] = await list(
    'messages',
    conversationsHistory
  )
  // Messages w/ a subtype are actions (joined, pinned, etc.), not actual messages
  const realMessages: MessageWithChannel[] = allMessages.filter(msg => !msg.subtype)
  // Slack API uses channel as a parameter, so it's not in the response, but we still want to save it
    .map((msg) => ({
      ...msg,
      channel: id
    }))
  return realMessages
}

export async function getChannelMessages(channel: Channel): Promise<MessageWithChannel[]> {
  try {
    if (!channel.id) throw new Error('Channel ID missing from object.')
    try {
      return await getMessages(channel.id)
    } catch (err: any) {
      if ((err?.data || err?.response)?.error === 'not_in_channel') {
        if (channel.is_archived) return [] // Can't join to get messages
        await app.client.conversations.join({ token, channel: channel.id })
        return await getMessages(channel.id)
      }
      throw err
    }
  } catch (err: any) {
    err.channel = channel
    throw err
  }
}

export const job = async (): Promise<MessageWithChannel[]> => await getChannels()
  .then(async channels => await Promise.all(channels.map(getChannelMessages)))
  .then(messages => messages.flat())
