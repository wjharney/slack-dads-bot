import { Channel } from '@slack/web-api/dist/response/UsersConversationsResponse'

import { job as getIdleUsers } from './idle-users'
import { app, ensureEnv, list } from '../util'
import { apiWorkaround } from '../util/apiWorkaround'

const CHANNEL_FOR_IDLING = ensureEnv('CHANNEL_FOR_IDLING')

const getKickableChannelIds = (channels: Channel[]): string[] => {
  const ids: string[] = []
  for (const channel of channels) {
    if (!channel.is_general && channel.id && channel.id !== CHANNEL_FOR_IDLING) ids.push(channel.id)
  }
  return ids
}

export const job = async (): Promise<void> => {
  for (const { id: user } of await getIdleUsers()) {
    const userChannels = await list(
      'channels',
      async (extra) => await app.client.users.conversations({ user, ...extra })
    )

    const channelsToKickFrom = getKickableChannelIds(userChannels)
    // There's nothing to do if they're not in any kickable channels
    if (channelsToKickFrom.length === 0) continue

    console.log(`Kicking ${user} from ${channelsToKickFrom.length}`)

    for (const channel of channelsToKickFrom) {
      await apiWorkaround('conversations.kick', new URLSearchParams({ user, channel }))
    }

    if (userChannels.every(ch => ch.id !== CHANNEL_FOR_IDLING)) {
      await app.client.conversations.invite({
        users: user,
        channel: CHANNEL_FOR_IDLING
      })
    }

    await app.client.chat.postMessage({
      channel: CHANNEL_FOR_IDLING,
      text: `Hey <@${user}>, you may have noticed that you've been removed from a few channels. We do this periodically
      to ensure our channels aren't filled with identity-stealing lurker bots. You haven't posted for a while, so you
      got swept up with them. But you're reading this, so hopefully that means you're back. Yay! Feel free to click
       \`Add channels\` in the sidebar to re-join the conversation. We can't wait to hear from you!`
        .replace(/\n\s+/g, ' ')
    })
  }
}
