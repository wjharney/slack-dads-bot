import { job as getIdleUsers } from './idle-users'
import { app, ensureEnv, list } from '../util'
import { Channel } from '@slack/web-api/dist/response/ChannelsListResponse'

const CHANNEL_FOR_IDLING = ensureEnv('CHANNEL_FOR_IDLING')

const getKickableChannelIds = (channels: Channel[]): string[] => {
  const ids: string[] = []
  for (const channel of channels) {
    if (!channel.is_general && channel.id && channel.id !== CHANNEL_FOR_IDLING) ids.push(channel.id)
  }
  return ids
}

export const job = async (): Promise<void> => {
  for (const { id: user } of await getIdleUsers()) {
    const userChannels = await list(
      'channels',
      async (extra) => await app.client.users.conversations({ user, ...extra })
    )

    const channelsToKickFrom = getKickableChannelIds(userChannels)
    // There's nothing to do if they're not in any kickable channels
    if (channelsToKickFrom.length === 0) continue

    if (userChannels.every(ch => ch.id !== CHANNEL_FOR_IDLING)) {
      await app.client.conversations.invite({
        users: user,
        channel: CHANNEL_FOR_IDLING
      })
    }

    await app.client.chat.postMessage({
      channel: CHANNEL_FOR_IDLING,
      text: `Hey <@${user}>, how's it going? We noticed that you haven't posted anything in a while, and we'd love for
      you to participate! If you don't, we'll assume that you're on a break from us, and hopefully you won't be offended
      if we take a break from you. Don't worry, we won't ban you, we'll just remove you from a few channels that you can
      re-join at any time. We do this from time to time because we are a support group, after all. It's easier for our
      members to open up to a group of peers, rather than a group of peers, bots, and lurkers. Hope to hear from you soon!`
        .replace(/\n\s+/g, ' ')
    })
  }
}
