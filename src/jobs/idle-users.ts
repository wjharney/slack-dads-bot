import { Member } from '@slack/web-api/dist/response/UsersListResponse'
import { readJsonSync } from 'fs-extra'

import { job as getUsers } from './users'
import { job as getDadlist } from './active-members'

// Users who aren't in the active member channel, but still shouldn't be considered idle
const EXTRA_ACTIVE_USERS = (() => {
  const file = './extra-active-users.json'
  try {
    return new Set<string>(readJsonSync(file))
  } catch {
    console.error(`Please include a list of active user IDs in ${file}. It can be an empty list.`)
    process.exit(1)
  }
})()

export const job = async (): Promise<Array<Member & {id: string}>> => {
  const users = await getUsers()
  const dadlist = new Set(await getDadlist())
  const isIdleUserWithId = (user: Member): user is Member & {id: string} => {
    return !user.is_bot && typeof user.id === 'string' && !dadlist.has(user.id) && !EXTRA_ACTIVE_USERS.has(user.id)
  }
  return users.filter(isIdleUserWithId)
}
