import { UsersListResponse } from '@slack/web-api'
import {
  app,
  list
} from '../util'

export const job = async (includeDeleted = false): Promise<NonNullable<UsersListResponse['members']>> => {
  const users = await list(
    'members',
    app.client.users.list
  )
  return includeDeleted ? users : users.filter(user => !user.deleted)
}
