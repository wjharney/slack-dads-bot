import { app } from '../util'
import { job as getAllChannels } from './channels'

// Join all available channels
export const job = async (): Promise<void> => {
  for (const channel of await getAllChannels({
    exclude_archived: true,
    limit: 1000
  })) {
    if (!channel.is_member) {
      if (channel.id) {
        await app.client.conversations.join({
          channel: channel.id
        }).catch(console.error)
        console.log(`Joined channel ${channel.name ?? channel.id}`)
      } else {
        console.log('Cannot join channel with missing ID', channel)
      }
    }
  }
}
