if (typeof process.env.XOXC !== 'string') {
  console.error(`Missing XOXC environment variable for Slack user token.
    To get the user token, open Slack in browser and execute:
    ((cfg)=>console.log(cfg.teams[cfg.lastActiveTeamId].token))(JSON.parse(localStorage.localConfig_v2))`)
  process.exit(1)
}
if (typeof process.env.XOXD !== 'string') {
  console.error(`Missing XOXD environment variable for Slack "d" cookie.
    To get the cookie value, open Slack in browser and navigate to the DevTools Storage panel.`)
  process.exit(1)
}
const { XOXC, XOXD } = process.env

if (XOXC || XOXD) {
  console.error("Couldn't quite get `apiWorkaround` to work reliably. Try running the job in browser, instead.")
  process.exit(1)
}

/**
 * Used to execute Slack API requests that are available to browser users, but not to free-tier bots
 * @param method Slack API method name, see https://api.slack.com/methods
 * @param params URL parameters for the method, if needed
 * @param body Payload for the method, if needed. XOXC token is optional as it is automatically added
 * @returns Plain JSON response object, not the typical fancy object from the Slack SDK
 */
export async function apiWorkaround(method: string, params?: URLSearchParams, body?: FormData): Promise<unknown> {
  if (!body) body = new FormData()
  if (!body.has('token')) body.append('token', XOXC)
  const response = await fetch(`https://app.slack.com/api/${method}${params ? '?' + params.toString() : ''}`, {
    method: 'POST',
    body,
    headers: { Cookie: `d=${XOXD};` }
  })
  if (response.status !== 200) {
    const err = new Error(`${method}: ${response.status} ${response.statusText}`)
    throw Object.assign(err, { response })
  }
  const json = await response.json()
  if (!json.ok) {
    const err = new Error(json.error ?? 'Something broke?')
    throw Object.assign(err, { response, json })
  }
  return json
}
