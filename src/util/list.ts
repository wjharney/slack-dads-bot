import { CursorPaginationEnabled, Method, TokenOverridable, WebAPICallOptions, WebAPICallResult } from '@slack/web-api'
import { token } from './app'

interface ListMethodArguments extends WebAPICallOptions, TokenOverridable, CursorPaginationEnabled {}

type WebAPICallResultWithList<Key extends string> = WebAPICallResult & {
  [K in Key]?: unknown[]
}

export async function list<Key extends string, Response extends WebAPICallResultWithList<Key>>(
  key: Key,
  func: Method<ListMethodArguments, Response>
): Promise<NonNullable<Response[Key]>>
export async function list(
  key: string,
  func: Method<ListMethodArguments, WebAPICallResult>
): Promise<unknown[]>
export async function list<Key extends string, Response extends WebAPICallResultWithList<Key>>(
  key: Key,
  func: Method<ListMethodArguments, Response>
): Promise<unknown[]> {
  const result: unknown[] = []
  let cursor: string | undefined = ''
  do {
    const batch: Response = await func({ cursor, token })
    if (!batch.ok) {
      const err = new Error('Not OK')
      ;(err as Error & { response: Response }).response = batch
      throw err
    }

    const arr = batch[key]
    if (Array.isArray(arr)) {
      result.push(...arr)
    } else {
      throw new Error(`Property ${key.toString()} on response is not an array.`)
    }
    cursor = batch.response_metadata?.next_cursor
  } while (cursor != null && cursor !== '')

  return result
}
