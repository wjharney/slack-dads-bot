import fse from 'fs-extra'
import path from 'path'

export const REPORTS = path.resolve(__dirname, '../../reports')

function guard(filename: string): void {
  if (path.basename(filename) !== filename) {
    throw new Error('Filename cannot be a path.')
  }
}

export async function exists(filename: string): Promise<boolean> {
  guard(filename)
  return await fse.pathExists(path.join(REPORTS, filename))
}

export function existsSync(filename: string): boolean {
  guard(filename)
  return fse.pathExistsSync(path.join(REPORTS, filename))
}

export async function read(filename: string, options?: string | fse.ReadOptions): Promise<any> {
  guard(filename)
  const file = path.join(REPORTS, filename)
  const ext = path.extname(filename).toLowerCase()
  if (ext === '.json') {
    if (typeof options === 'string') {
      throw new TypeError('Options cannot be string for reading JSON file.')
    }
    return await fse.readJson(file, options)
  } else {
    if (options && typeof options !== 'string') {
      throw new TypeError('Options for plain files must be a string encoding.')
    }
    return await fse.readFile(file, options ?? 'utf8')
  }
}

// Pretty sure some of this type gymnastics is because fs-extra is v10, but @types/fs-extra is v9...
export function readSync(filename: `${string}.json`, options?: Exclude<fse.BufferEncodingOption | fse.ReadOptions, 'buffer'>): any
export function readSync(filename: string, options?: BufferEncoding | Pick<fse.ReadOptions, 'encoding' | 'flag'>): any
export function readSync(filename: string, options?: Exclude<BufferEncoding | fse.BufferEncodingOption | fse.ReadOptions, 'buffer'>): any {
  guard(filename)
  const file = path.join(REPORTS, filename)
  const ext = path.extname(filename).toLowerCase()
  if (ext === '.json') {
    if (typeof options === 'string') {
      throw new TypeError('Options cannot be string for reading JSON file.')
    }
    return fse.readJsonSync(file, options)
  } else {
    if (options && typeof options !== 'string') {
      throw new TypeError('Options for plain files must be a string encoding.')
    }
    return fse.readFileSync(file, options ?? 'utf8')
  }
}

export async function write(filename: string, data: unknown, options?: fse.WriteOptions): Promise<void> {
  guard(filename)
  const file = path.join(REPORTS, filename)
  if (typeof data === 'string') {
    await fse.outputFile(file, data, options ?? ('utf8' as const))
  } else {
    await fse.outputJson(file, data, options ?? { encoding: 'utf8', spaces: 2 })
  }
}

export function writeSync(filename: string, data: unknown, options?: fse.WriteOptions): void {
  guard(filename)
  const file = path.join(REPORTS, filename)
  if (typeof data === 'string') {
    fse.outputFileSync(file, data, options)
  } else {
    fse.outputJsonSync(file, data, options ?? { encoding: 'utf8', spaces: 2 })
  }
}
