import { SlackEventMiddlewareArgs } from '@slack/bolt'
import { app, ensureEnv, token } from '../../../util'

const alertChannel = ensureEnv('CHANNEL_FOR_FUN_DADMIN_ALERTS')

/**
 * Handle a Slack channel created event
 * @param event Slack event object
 */
export const onChannelCreated = async ({ event }: SlackEventMiddlewareArgs<'channel_created'>): Promise<void> => {
  // node console doesn't resolve user mention, but let's just pretend!
  console.log(`Channel #${event.channel.name} created by <@${event.channel.creator}>`)
  await app.client.chat.postMessage({
    channel: alertChannel,
    icon_emoji: 'slack',
    text: `New channel: <#${event.channel.id}>. Thanks, <@${event.channel.creator}>!`,
    token
  })
  await app.client.conversations.join({
    channel: event.channel.id,
    token
  })
}
