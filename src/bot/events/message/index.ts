import { SlackEventMiddlewareArgs } from '@slack/bolt'
import { trackLastMessage } from './trackLastMessage'
import { watchBots } from './watchBots'

export const onMessage = async (event: SlackEventMiddlewareArgs<'message'>): Promise<void> => {
  const results = await Promise.allSettled([
    trackLastMessage(event),
    watchBots(event)
  ])

  const rejections = results.filter((r): r is PromiseRejectedResult => r.status === 'rejected')
  if (rejections.length) {
    throw new AggregateError(rejections.map(r => r.reason))
  }
}
