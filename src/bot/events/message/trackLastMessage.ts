import { SlackEventMiddlewareArgs, MessageEvent } from '@slack/bolt'
import { Action, ActionAttributes, validBody } from '../../../util'

const shouldHandle = (event: MessageEvent): event is Exclude<MessageEvent, { subtype: string }> => {
  return typeof event.channel === 'string' && !event.subtype
}

/**
 * Tracks the last message sent by each user.
 */
export const trackLastMessage = async ({ body, message }: SlackEventMiddlewareArgs<'message'>): Promise<void> => {
  if (!shouldHandle(message) || !validBody(body)) {
    return // Don't handle this event
  }
  const parsed: ActionAttributes = {
    action: 'message',
    team: body.team_id,
    user: message.user,
    timestamp: new Date(Number(message.ts) * 1000)
  }
  console.log(`[${parsed.timestamp.toISOString()}] ${parsed.action}: ${parsed.team}/${parsed.user}`)
  await Action.upsert(parsed)
}
