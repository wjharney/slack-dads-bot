import { SlackEventMiddlewareArgs } from '@slack/bolt'
import { app, ensureEnv } from '../../../util'

const CHANNEL_FOR_BOTS = ensureEnv('CHANNEL_FOR_BOTS')
const CHANNEL_FOR_ACTIVE_MEMBERS = ensureEnv('CHANNEL_FOR_ACTIVE_MEMBERS')

export async function watchBots({ message }: SlackEventMiddlewareArgs<'message'>): Promise<void> {
  if (message.subtype === 'channel_join' && message.channel === CHANNEL_FOR_BOTS) {
    await app.client.chat.postMessage({
      channel: CHANNEL_FOR_ACTIVE_MEMBERS,
      text: `<@${message.user}> joined <#${message.channel}>. Is <@${message.user}> a bot?`
    })
  }
}
